package ge.sct.microservices.internet.api;

import ge.sct.microservices.internet.model.Internet;
import ge.sct.microservices.internet.model.Page;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author stavares
 */
@RestController
public class Api {

  private Map<Integer, Internet> internets;
  private static final Logger LOG = Logger.getLogger(Api.class.getName());

  public Api() {
    internets = new HashMap<>();
    internets.put(1,new Internet(1,"https://s3.amazonaws.com/ge.inet1.foo.bar.com/internet1.json"));
    internets.put(2,new Internet(2,"https://s3.amazonaws.com/ge.inet1.foo.bar.com/internet2.json"));
  }

  @RequestMapping("/pages/{id}")
  public List<Page> getPages(@PathVariable("id") Integer id) throws IOException{
    return new ArrayList<>(internets.get(id).getPages().values());
  }
}
