package ge.sct.microservices.internet.model;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;

/**
 *
 * @author stavares
 */
public class Internet {
  private final int id;
  private final String url;
  private final Map<String, Page> pages = new HashMap<>();
  private static final Logger LOG = Logger.getLogger(Internet.class.getName());

  public Internet(int id, String url) {
    this.id = id;
    this.url = url;
  }

  public int getId() {
    return id;
  }

  public String getUrl() {
    return url;
  }

  public Map<String, Page> getPages() throws IOException {

    if(this.pages.isEmpty()){

      String json = Jsoup.connect(this.url).ignoreContentType(true)
          .execute().body();
      JSONObject obj = new JSONObject(json);
      JSONArray pageObjects = obj.getJSONArray("pages");

      for (int i = 0; i < pageObjects.length(); i++) {
        Page page = new Page(pageObjects.getJSONObject(i).getString("address"));
        JSONArray linkObjects = pageObjects.getJSONObject(i).getJSONArray("links");
        for (int j = 0; j < linkObjects.length(); j++) {
          page.getLinks().add(linkObjects.getString(j));
        }
        this.pages.put(page.getAddress(), page);
      }
    }
    return Collections.unmodifiableMap(this.pages);
  }
}
