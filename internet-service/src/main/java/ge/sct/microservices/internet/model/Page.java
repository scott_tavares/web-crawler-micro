package ge.sct.microservices.internet.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author stavares
 */
public class Page {

  private String address;
  private List<String> links = new ArrayList();
  private static final Logger LOG = Logger.getLogger(Page.class.getName());

  public Page() {
  }

  public Page(String address) {
    this.address = address;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public List<String> getLinks() {
    return links;
  }

  public void setLinks(List<String> links) {
    this.links = links;
  }

}
